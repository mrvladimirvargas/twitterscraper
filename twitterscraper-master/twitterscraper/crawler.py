from query import query_tweets
import datetime as dt
import time
import json

limite = 100000

delta_t = dt.timedelta(days = 15)
fecha_inicial = dt.date(2014, 1, 1)
fecha_final = fecha_inicial + delta_t

path = 'tweets_rcn/{0}.json'
user = "NoticiasRCN"
periods = 0

while fecha_final < dt.date.today():
    print('From', fecha_inicial, 'to', fecha_final)

    # Crawl tweets
    list_of_tweets = query_tweets(user, limite, begindate=fecha_inicial, enddate=fecha_final, poolsize=15)

    #Update dates
    fecha_inicial = fecha_final
    fecha_final = fecha_final + delta_t
    periods += 1

    # Save in json format
    list_of_json = []
    for tweets in list_of_tweets: # for looping
        tweets.timestamp = dt.datetime.strftime(tweets.timestamp, '%Y-%m-%d %H:%M:%S')
        list_of_json.append(vars(tweets))
    filepath = path.format(periods)
    print(len(list_of_json), 'tweets being saved at', filepath)
    with open(filepath, "w") as jsonfile:
        json.dump(list_of_json, jsonfile)

    # Sleep for half a minute in order not to raise suspicion
    time.sleep(30)